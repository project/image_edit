<?php

namespace Drupal\image_edit\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an overlay for the image-editor to operate in.
 *
 * @Block(
 *   id = "image_edit",
 *   admin_label = @Translation("Image-edit block"),
 *   category = @Translation("Editor"),
 * )
 */
class ImageEditorOverlay extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The app root.
   *
   * @var string
   */
  protected $appRoot;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * Constructs a ImageEditorOverlay object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param string $app_root
   *   The app root.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The module extension list.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $app_root, AccountProxyInterface $current_user, ModuleExtensionList $extension_list_module) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->appRoot = $app_root;
    $this->currentUser = $current_user;
    $this->moduleExtensionList = $extension_list_module;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('app.root'),
      $container->get('current_user'),
      $container->get('extension.list.module')
    );
  }

  /**
   * Builds and returns the renderable array for this block plugin.
   *
   * If a block should not be rendered because it has no content, then this
   * method must also ensure to return no content: it must then only return an
   * empty array, or an empty array with #cache set (with cacheability metadata
   * indicating the circumstances for it being empty).
   *
   * @return array
   *   A renderable array representing the content of the block.
   *
   * @see \Drupal\block\BlockViewBuilder
   */
  public function build() {
    $blockContent = [];

    if ($this->currentUser->hasPermission('use image edit')) {
      $blockContent = [
        '#type' => 'item',
        '#markup' => file_get_contents(
          $this->appRoot . DIRECTORY_SEPARATOR .
          $this->moduleExtensionList->getPath('image_edit') .
          DIRECTORY_SEPARATOR . 'popup_template.html'
        ),
        '#attached' => ['library' => ['image_edit/editor_code']],
        '#allowed_tags' => [
          'canvas', 'button', 'div', 'h2', 'label', 'input', 'nav', 'p', 'a',
        ],
      ];
    }

    return $blockContent;
  }

}
