<?php

namespace Drupal\image_edit\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileSystem;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for the image_edit.replace_image route.
 */
class ReplaceImage extends ControllerBase {

  /**
   * File ssytem service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * The storage handler class for files.
   *
   * @var \Drupal\file\FileStorage
   */
  private $fileStorage;

  /**
   * Constructs a ReplaceImage object.
   *
   * @var \Drupal\Core\File\FileSystem $fileSystem
   *   File system service.
   */
  public function __construct(FileSystem $fileSystem) {
    $this->fileSystem = $fileSystem;
    $this->fileStorage = $this->entityTypeManager()->getStorage('file');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('file_system'));
  }

  /**
   * Replaces the image.
   *
   * @var \Symfony\Component\HttpFoundation\Request $request
   *   Request object
   * @var integer $fileId
   *   File ID.
   */
  public function replaceImage(Request $request, $fileId) {
    $image = $this->fileStorage->load($fileId);

    if (is_null($image)) {
      return new AjaxResponse(['msg' => $this->t('Image not found.')]);
    }

    $file = new \SplFileObject(
      $this->fileSystem->realpath($image->getFileUri()), 'wb'
    );
    $fileContent = base64_decode($request->getContent());

    if (!$fileContent) {
      return new AjaxResponse(0, 404);
    }

    $bytesWritten = $file->fwrite($fileContent);
    image_path_flush($image->getFileUri());

    return new AjaxResponse($bytesWritten);
  }

}
